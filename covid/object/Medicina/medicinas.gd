extends CharacterBody2D


var speed = 260.0

var med := preload("res://object/Medicina/health_med.tscn")

@onready var anim := $AnimationPlayer

func _process(_delta):
	anim.play("med")

func _physics_process(_delta):
	velocity.x = -speed 
	move_and_slide()


func _on_area_2d_body_entered(body):
	if body is Player:
		
		var h = med.instantiate()
		get_tree().current_scene.add_child(h)
		
		body.get_node("gui/PlayerUI").health_load()
		queue_free()


func _on_visible_on_screen_notifier_2d_screen_exited():
	queue_free()
