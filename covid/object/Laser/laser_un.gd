class_name Laser_un
extends Area2D

@export var speed := 200


func _process(_delta):
	
	global_position.x += speed * _delta

func _on_body_entered(body):

	if body is Covid:
		body.dead()
		queue_free()
	elif body is CovidVirus:
		
		body.get_node("Bar").dead()
		queue_free()
	
	
func _on_visible_on_screen_notifier_2d_screen_exited():
	queue_free()
