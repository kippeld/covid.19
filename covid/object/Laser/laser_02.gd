extends Area2D

@onready var anim := $AnimationPlayer
var index = null
var red : bool = false


func _process(_delta):
	anim.play("drone")
	if index:
		
		index.ui_ad()
		if !red:
			red = true
			
			index.get_node("gui/PlayerUI").Health()
			await get_tree().create_timer(1).timeout
			red = false
			
			
func _on_body_entered(body):
	if body is Player:
		index = body


func _on_body_exited(body):
	if body is Player:
		index = null
