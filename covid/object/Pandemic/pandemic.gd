extends CharacterBody2D

@export var speed = 200
var index : bool = false

var hp : int = 5

@onready var anim := $AnimationPlayer

var pot := preload("res://object/Pandemic/potion.tscn")

func _ready():
	rotation = deg_to_rad(randf_range(0, 360))
	
	
func _process(_delta):
	anim.play("pand")

func _physics_process(_delta):
	velocity.x = -speed 
	
	move_and_slide()


func _on_pand_body_entered(body):
	
	if body is Player:
		#audio.play()
		var h = pot.instantiate()
		get_tree().current_scene.add_child(h)
		
		body.get_node("gui/ui").health(hp)
		queue_free()


func _on_visible_on_screen_notifier_2d_screen_exited():
	queue_free()
