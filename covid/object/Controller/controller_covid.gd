class_name ControllerCovid
extends Node2D

signal top_bar()


var covid = [preload("res://object/Covid/covid_un.tscn"),
			preload("res://object/Covid/covid_dos.tscn"),
			preload("res://object/Covid/covid_tres.tscn"),
			preload("res://object/Covid/covid_q.tscn")]
   
var pandem := preload("res://object/Pandemic/pandemic.tscn")

var medicinas := preload("res://object/Medicina/medicinas.tscn")

var covid_virus := preload("res://object/CovidUn/covid_virus.tscn")

@onready var player := get_tree().current_scene.get_node("Player")
#################################################################################
@onready var med_timer := $MedicinasTimer
@onready var pad_timer := $PandemicTimer
@onready var timer := $Timer
@onready var covid_timer := $CovidTimer
@onready var tots_timer := $TotsTimer
#################################################################################

#var back := ["#D5F5E3", "#BB8FCE", "#ff0000", "#00ff00", "#0000ff"]

### 5
@export var bark_1 : Color
@export var bark_2 : Color
@export var bark_3 : Color
@export var bark_4 : Color
@export var bark_5 : Color

@onready var backgroud = $Background/Backgroud1
var covid_e = null
################################################################################
var back_index : int = 0
var font : int = 0


var index_covid : Array = [0,1,2, 3, 1]

var index : int = 0
################################################################################
var ui_index : int = 0 
var timer_wait_time : Array = [1.2, 0.9, 0.7, 0.5, 0.3 ]
 

func _ready():

	Global.I_PUNTS = 0
	Global.i_med_timer = true
	Global.i_pandemic_time = false
	
	
	backgroud.modulate = "#D5F5E3"
	
	global_player()
	
	timer.wait_time = timer_wait_time[ui_index]
	
	covid_timer.wait_time = Global.I_COVID_TIMER
	
	backgroud.modulate = bark_1
	
	


func global_player() -> void:	
	if Global.t_players == Global.T_PLAYER.PLAYER:
		med_timer.stop()
		pad_timer.stop()
		timer.stop()
		covid_timer.stop()
	elif Global.t_players == Global.T_PLAYER.RUN:
		
		med_timer.start()
		pad_timer.start()
		timer.start()
		covid_timer.start()
		
	elif Global.t_players == Global.T_PLAYER.VIRUS:
		timer.stop()
		pad_timer.stop()
		covid_timer.stop()
	elif Global.t_players == Global.T_PLAYER.TOTS:
		med_timer.stop()
		pad_timer.stop()
		timer.stop()
		covid_timer.stop()
		tots_timer.start()
		
		await get_tree().create_timer(20).timeout
		emit_signal('top_bar')
		#Global.t_players = Global.T_PLAYER.TOP
		
## 110, 560
func covids():
	
	randomize()
	
	#var t = 4
	
	#print()
		
	
	var h = covid[index_covid[index]].instantiate()
	add_child(h) 	
	
	var i = randf_range(0.2, 0.4)	
	h.scale_bar(i)
	
	
	h.scale = Vector2(i, i)
	h.global_position = $Marker2D.global_position
	h.global_position.y = randf_range(Global.controller_position_y['min'], Global.controller_position_y['max'])


func covids_tots() -> void:
	
	randomize()
	
	var r = randf_range(0, covid.size() )
	var h = covid[r].instantiate()
	add_child(h) 	
	
	h.tots()
	
	var i = randf_range(0.2, 0.4)	
	h.scale_bar(i)
	
	h.scale = Vector2(i, i)
	h.global_position = $Marker2D.global_position
	h.global_position.y = randf_range(Global.controller_position_y['min'], Global.controller_position_y['max'])
	



func _process(_delta):
	
	if Global.t_players == Global.T_PLAYER.VIRUS:
		
		if covid_e == null:
			Global.t_players = Global.T_PLAYER.TOTS
			self.global_player()
	
	

func _on_timer_timeout():
	covids()


func _on_pandemic_timer_timeout():
	if Global.i_pandemic_time:
		return
	##
	
	randomize()
	var p = pandem.instantiate()
	add_child(p)
	
	
	pad_timer.wait_time = randf_range(4, 8)
	p.global_position = $Marker2D.global_position
	p.global_position.y = randf_range(Global.controller_position_y['min'], Global.controller_position_y['max'])




func _on_medicinas_timer_timeout():
	if Global.i_med_timer:
		return
	
	randomize()
	med_timer.wait_time = randf_range(20, 50)
	
	var p = medicinas.instantiate()
	add_child(p) 
	p.global_position = $Marker2D.global_position
	p.global_position.y = randf_range(Global.controller_position_y['min'], Global.controller_position_y['max'])


func _color_back_index() -> void:
	back_index += 1
	if back_index == 1:
		backgroud.modulate = bark_2
	elif back_index == 2:
		backgroud.modulate = bark_3
	elif back_index == 3:
		backgroud.modulate = bark_4
	elif back_index == 4:
		backgroud.modulate = bark_5

func _on_covid_timer_timeout():
	
	
	ui_index += 1
	
	
	
	
	if timer_wait_time.size() > ui_index:
		timer.wait_time = timer_wait_time[ui_index]
	
	
	self._color_back_index()
	#print(back_e)
	#if back.size() != back_index+1:
	#	pass
	#	back_index += 1
	#	backgroud.modulate = back[back_index]
	
	if Global.FONT_TO_SIZE != font+1:
		font += 1
		player.get_node("gui/ui").text_font()
	
	
	if index_covid.size() != index +1:
		index += 1
	
	#if covid.size() != index+1:
	#	index += 1
		
	if ui_index == 5:
		
		Global.t_players = Global.T_PLAYER.VIRUS
		
		global_player()
		covid_e = covid_virus.instantiate()
		await get_tree().create_timer(8).timeout
		add_child(covid_e) 
		covid_e.global_position = $Marker2D.global_position


func _on_tots_timer_timeout():
	self.covids_tots()
	


func _on_player_player_controller_covid():
	self.global_player()
