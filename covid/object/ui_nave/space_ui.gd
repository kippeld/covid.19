extends Control


var mod = "#ffffff57"
var mod_old = "#ffffffff"

var nave_nave : Array = [["res://images/space/player_02.png", "res://images/space/player_01.png", "res://images/space/player_03.png"],
						 ["res://images/space/nave_02.png", "res://images/space/nave_01.png", "res://images/space/nave_03.png"]]

var space_str = null

var k = 0   ### todo

###########################################################################################
#signal nave_red(nave, red)
#var red_int : int = 0
#var nave_str : String = "nave_01"
###########################################################################################

#@onready var space_ui := $space_ui

func _ready():
	
	space_str = $Control
	
	#for e in range(3):
		
	## top
	
	$Control.get_node("nave/Nave01").texture = load("res://images/space/player_01.png")
	$Control.get_node("nave/Nave01").visible = true
	
	$Control.get_node("nave/Nave02").texture = load("res://images/space/player_02.png")
	$Control.get_node("nave/Nave02").visible = false
	
	$Control.get_node("nave/Nave03").texture = load("res://images/space/player_03.png")
	$Control.get_node("nave/Nave03").visible = false
	######################################################################################
	
	$Control2.get_node("nave/Nave01").texture = load("res://images/space/nave_01.png")
	$Control2.get_node("nave/Nave01").visible = true
	
	$Control2.get_node("nave/Nave02").texture = load("res://images/space/nave_02.png")
	$Control2.get_node("nave/Nave02").visible = false
	
	$Control2.get_node("nave/Nave03").texture = load("res://images/space/nave_03.png")
	$Control2.get_node("nave/Nave03").visible = false
	
	$Control2.modulate = "#ffffff57"
	
	
	####################################################################################
	$Control.get_node("nave").position.x = 170
	$Control2.get_node("nave").position.x = 120

	var i : int = 1
	for r in get_children():
		r.ui_label("Space 0" + str(i))
		i += 1

func space_top():
	for e in get_children():
		e.modulate = mod


func _input(event):

						
	if event is InputEventMouseButton:

		if not event.pressed:
			#print(get_children())
			var r = 0
			for e in get_children():
				if e.get_node("rect").get_rect().has_point(e.get_node("rect").get_local_mouse_position()):
					self.space_top()
					k = r	
					e.modulate = "#ffffffff"
					space_str = e
					## todo
					GlobalNave.data_k(k)	
					break
					
				r += 1


func _on_nave_zero_nave_red(optional):
	#print(optional)
	for e in get_children():
		e.nave_e(optional)
	
	
	GlobalNave.data_nave(optional)		
	##############################################
	
	#nave_str = optional
	#emit_signal('nave_red', red_int, nave_str)
