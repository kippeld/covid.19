extends Control

var names : Array = ["nave_01", "nave_02", "nave_03"]

func _ready():
	$nave/Nave01.visible = false
	$nave/Nave02.visible = false
	$nave/Nave03.visible = false

func ui_label(ui : String) -> void:
	$rect/Label.text = ui


func nave_e(optional) -> void:
	
	for i in $nave.get_children():
		i.visible = false
	
	if names[0] == optional:
		$nave/Nave01.visible = true
	elif names[1] == optional:
		$nave/Nave02.visible = true
	elif names[2] == optional:
		$nave/Nave03.visible = true

