extends Control


#@export  var nave : Node = null

@onready var nave_dos := $nave_dos

var color_bar = null

var names : Array = ["nave_01", "nave_02", "nave_03"]
################################################################################
var index_nave : int = 0
################################################################################

var ui_color : Array = ["#57f9ac", "#57a0f9", "#f55a62"] #,["#289635", "#287896", "#962828"]]

signal nave_red(optional)


func _reds(nave) -> void:
	
	color_bar = $nave_dos/color_nave
	
	for e in nave.get_children():
		e.get_node("color").visible = false

func _ready():
	
	var e : int = 0
	for i in nave_dos.get_children():
		i.get_node("rect").color = ui_color[e]
		
		if e == 0:
			i.get_node("color").visible = true
		else:
			i.get_node("color").visible = false
				
		e += 1



func _block() -> void:
	
	var k = 0
	for e in nave_dos.get_children():
		if e.get_node("rect").get_rect().has_point(e.get_node("rect").get_local_mouse_position()):
			if e != color_bar:
				self._reds(nave_dos)
				e.get_node("color").visible = true
				color_bar = e
				
				emit_signal('nave_red', names[k])
				
		k += 1


func _input(event):
		
	if event is InputEventMouseButton:
			
		if not event.pressed:
			
			self._block()

