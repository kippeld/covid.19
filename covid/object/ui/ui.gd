extends CanvasLayer

@onready var covid_font := $Label
@onready var texture := $TextureProgressBar

@onready var nom := $ui_control/nom


@export var color_1 : Color 
@export var color_2 : Color
@export var color_3 : Color
@export var color_4 : Color
@export var color_5 : Color

var font : Array = ["Alpha", "Beta", "Gamma", "Delta", "Omicron"]
var indexFont = 0

var ui_noms :int = 0


func _ready():
	Global.FONT_TO_SIZE = font.size()
	covid_font.text = font[0] 
	texture.tint_progress = color_1
	

func text_font() -> void:
	
	indexFont+=1
	covid_font.text = font[indexFont]
 

func ui_nom() -> void:
	ui_noms += 1
	nom.text = str(ui_noms)

func ui_red() -> bool:
	return texture.value == 0

func health(hp : int) -> void:
	#print(texture.value)
	texture.value += hp
	
	if texture.value <= 20:
		texture.tint_progress = color_1
		Global.I_PUNTS = 1
	elif texture.value <= 40:
		texture.tint_progress = color_2
		Global.I_PUNTS = 2
	elif texture.value <= 60:
		texture.tint_progress = color_3
		Global.I_PUNTS = 3
	elif texture.value <= 80:
		texture.tint_progress = color_4
		Global.I_PUNTS = 4
	else:
		texture.tint_progress = color_5
		
	if texture.value == 100:
		Global.i_pandemic_time  = true



