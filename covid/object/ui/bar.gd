extends CanvasLayer


func _ready():
	visible = false


func _on_quit_pressed():
	get_tree().quit()


func _on_button_2_pressed():
	get_tree().paused = false
	visible = false
	get_tree().change_scene_to_file("res://world/nave.tscn")
	


func top_bar() -> void:
	visible = true
	get_tree().paused = true


func _on_player_ui_exit():
	visible = true
	get_tree().paused = true
