extends Sprite2D

@export var speed : float = 100
#@export var max : float  
@export var mins : float 

#   max    
# -1600    

func _process(_delta):
	#print(position.x)
	position.x -= speed * _delta
	if position.x < -mins:
		position.x += 2 * mins
