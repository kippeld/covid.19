class_name Covid
extends CharacterBody2D

@onready var anim := $AnimationPlayer
@onready var player := get_tree().current_scene.get_node("Player")
var expd = preload("res://object/Explosion/exp_2.tscn")

var speed = 200
####################################################################################################
@onready var coll := $Coll
@onready var area_coll := $Area2D/CollisionShape2D
####################################################################################################
enum States { IDLE, EXP }
var current = States.IDLE
####################################################################################################
var hp : float = 0.4

var anim_play : String = "covid"
####################################################################################################
var rotat_bool : bool


#func _ready():
#	anim_play = "covid"
#	await get_tree().create_timer(1.8).timeout
#	anim_play = "dead"

func scale_bar(h : float) -> void:
	#print(h)
	if h >= 0.2 and h <= 0.3: 	
		hp = 0.2 
	elif h > 0.3 and h <= 0.4: 
		hp = 0.3
	
	#print(hp)

func tots_old() -> void:
	anim_play = "dead_old"
	
func tots() -> void:
	#anim_play = "dead"
	anim_play = "covid"
	await get_tree().create_timer(1.8).timeout
	anim_play = "dead"

func _rotation_red(_delta) -> void:
	
	rotation_degrees += 30 * _delta
	
	if rotation_degrees > 360:
		rotation_degrees = 0
	#print(rotation_degrees)

func _physics_process(_delta : float):
	anim.play(anim_play)
	
	self._rotation_red(_delta)
	
	if States.IDLE == current:
		velocity.x = -speed 
		velocity.y += 2 * _delta
		move_and_slide()




func dead():
	if States.IDLE == current:
		current = States.EXP
		#print("foo bar")
		coll.visible = false
		area_coll.visible = false
		
		
		var h = expd.instantiate()
		
		get_tree().current_scene.add_child(h)
		#
		h.global_position = global_position
		h.scale = scale + Vector2(0.5, 0.5)
		
		var top = 8
		player.get_node("gui/ui").ui_nom()
		while 0 < top:
			await get_tree().create_timer(0.1).timeout
			
			if hp == 0.2:
				scale = scale - Vector2(0.02, 0.02)		
			else:
				scale = scale - Vector2(0.04, 0.04)
			
			top -= 1
			
		queue_free()


func _on_area_2d_body_entered(body):
	
	if body is Player:

		body.get_node("gui/PlayerUI").Health()
		
		self.dead()
	


func _on_visible_on_screen_notifier_2d_screen_exited():
	if anim_play == "covid":
		player.get_node("gui/ui").ui_nom()
	queue_free()
