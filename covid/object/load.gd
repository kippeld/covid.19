extends CanvasLayer

func _ready():
	visible = false

func _process(_delta):
	if Input.is_action_just_released("esc"):
		visible = true
		get_tree().paused = true

func _on_continua_pressed():
	visible = false
	get_tree().paused = false

func _on_quit_pressed():
	visible = false
	get_tree().paused = false
	get_tree().change_scene_to_file("res://world/menu.tscn")
