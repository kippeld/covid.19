extends CanvasLayer

@onready var pixel_health := $PixelHealth
var index = 0

var bar : bool = false

signal exit()
@onready var player := get_tree().current_scene.get_node("Player")

func health_load() -> void:
	
	if pixel_health.frame <= 1:
		pixel_health.frame = 0	
		Global.i_med_timer = true
	else:
		Global.i_med_timer = false
		pixel_health.frame -= 1
	

func Health() -> void:
	
	if pixel_health.frame < 4 and !bar:
		
		bar = true
		
		player.player_str = GlobalNave.e_flash
		
		Global.i_med_timer = false
		pixel_health.frame += 1
		await get_tree().create_timer(2).timeout
		player.player_str = GlobalNave.e_player
		bar = false
		
	if pixel_health.frame+1 > 4: 
		
		emit_signal('exit')
