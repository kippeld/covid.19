class_name Player
extends CharacterBody2D

var speed := 300.0

################################################################################
var laser_un = preload("res://object/Laser/laser_un.tscn")
var laser_sound = preload("res://object/sound/laser_sound.tscn")
################################################################################
var shoot_laset : bool = false

var punt : float

signal player_controller_covid()

@onready var player_blue := $PlayerBlue
@onready var coll := $CollisionShape2D

@onready var mark_01 := $PlayerBlue/Marker_01
@onready var mark_02 := $PlayerBlue/Marker_02
@onready var mark_03 := $PlayerBlue/Marker_03
################################################################################

var destination_old : Vector2 = Vector2(1152 + 200, 300)
var move_s : float = 100
################################################################################
@onready var mark_player = $"../mark/Mark_Player"
@onready var mark_player_o = $"../mark/Mark_Player_o"
################################################################################
@onready var anim := $AnimationPlayer
var player_str : String = "player_01"

func _ready():
	
	
	
	if GlobalNave.player_nave == "":
		player_blue.texture = load("res://images/space/player_01.png")
	else:
		player_blue.texture = load("res://images/space/"+GlobalNave.player_nave+ ".png")
		if GlobalNave.player_nave == "nave_01" or  GlobalNave.player_nave == "nave_02" or GlobalNave.player_nave == "nave_03":
			coll.position.y = 8
			mark_02.position.x = 85
			mark_02.position.y = 48
			
			mark_03.position.x = 20
			mark_03.position.y = 55
			
	global_position = mark_player.global_position
	coll.disabled = true
	############################################################################
	GlobalNave.data_flash()
	player_str = GlobalNave.e_player
	
	############################################################################
	Global.t_players = Global.T_PLAYER.PLAYER
	
	
			

func shoot() -> void:
	
	
	
	if Global.i_space_test:
		punt = 0.2
	else:	 
		punt = Global.I_PUNTS_ARRAY[Global.I_PUNTS]
	if !get_node("gui/ui").ui_red():

		if Input.is_action_just_pressed("Space") && !shoot_laset:
			#print("space")
			shoot_laset = true
			
			var h = laser_un.instantiate()
			
			get_tree().current_scene.add_child(h)	
			h.global_position = $PlayerBlue/Marker_01.global_position
			
			var sound = laser_sound.instantiate()
			add_child(sound)
			
			await get_tree().create_timer(punt).timeout
			shoot_laset = false


func _run_player(_delta) -> void:
	var dir = Vector2(Input.get_axis("ui_a", "ui_d"), Input.get_axis("ui_w", "ui_s"))
	
	velocity = dir * speed
	move_and_slide()
	
	shoot()

func _run_move_toward(_delta : float) -> void:
	#global_position = global_position.move_toward(destination, _delta * move_s)
	global_position = global_position.move_toward(mark_player_o.global_position, _delta * move_s)
	if global_position == mark_player_o.global_position:
		Global.t_players = Global.T_PLAYER.RUN
		#player_controller_covid
		coll.disabled = false
		emit_signal('player_controller_covid')

func _top_move_toward(_delta : float) -> void:
	global_position = global_position.move_toward(destination_old, _delta * move_s)
	if global_position == destination_old:
		get_node("gui/Bar").top_bar()
		Global.t_players = Global.T_PLAYER.PLAYER
		

func _physics_process(_delta : float):
	
	anim.play(player_str)
	
	if Global.t_players == Global.T_PLAYER.PLAYER:
		
		self._run_move_toward(_delta)
	elif Global.t_players == Global.T_PLAYER.RUN or Global.t_players == Global.T_PLAYER.VIRUS or Global.t_players == Global.T_PLAYER.TOTS:
	#set_physics_process(false)	
		self._run_player(_delta)
	elif Global.t_players == Global.T_PLAYER.TOP:
		self._top_move_toward(_delta)
	
	
func ui_ad() -> void:
	velocity.y += 50
	move_and_slide()
	


func _on_controller_covid_top_bar():
	Global.t_players = Global.T_PLAYER.TOP
	coll.disabled = true
