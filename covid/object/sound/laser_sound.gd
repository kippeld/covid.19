extends Node2D

@onready var audio := $AudioStreamPlayer2D

func _ready():
	audio.play()
	await get_tree().create_timer(0.8).timeout
	queue_free()


