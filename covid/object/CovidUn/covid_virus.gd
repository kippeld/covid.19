class_name CovidVirus
extends CharacterBody2D


enum Star {RIGHT, LEFT, RUN, WEAPON}
var stares = Star.RIGHT
var poss : = 50

@onready var anim := $AnimationPlayer
@onready var mark : Node2D = $mark
@onready var mark_up : Node2D = $mark_up

@onready var coll := $CollisionShape2D
#@export var red : Array = []

var index = 0
################################################################################
@onready var shooter := $Sprite2D/Shooter
@onready var shooter_timer := $ShooterTimer

enum Red { RUN_UN, RUN_DOS}
var reds = Red.RUN_UN


var virus = preload("res://object/Laser/laser_02.tscn")
var h = null
################################################################################
var play : String = "covid"
var move : float = 1

func _ready():
	global_position = mark_up.get_children()[0].position

func _physics_process(_delta):
	#print()
	if stares == Star.RIGHT:
		global_position = global_position.move_toward(mark.get_children()[index].position, poss * 3 * _delta)
		if global_position == mark.get_children()[index].position:
			stares = Star.RUN
			shooter_timer.wait_time = 5
			shooter_timer.start()
			#shooter_timer.set_wait_timer(5)
			
	elif stares == Star.RUN:
		global_position = global_position.move_toward(mark.get_children()[index].position, poss * _delta)
		if global_position == mark.get_children()[index].position:
			
			
			index = randf_range(0, mark.get_children().size())
	elif stares == Star.WEAPON:
		pass		
			
	elif stares == Star.LEFT:
		global_position = global_position.move_toward(mark_up.get_children()[1].position, poss * move * _delta)
		if global_position == mark_up.get_children()[1].position:
			queue_free()
		
	
func dead_old() -> void:
	play = "dead_old"
	move = 1
	await get_tree().create_timer(0.2).timeout
	
	
	var top = 5
	
	while 0 < top:
		move += 0.4
		await get_tree().create_timer(0.4).timeout
		
		top -= 1
	

func _process(_delta):
	anim.play(play)
	
	
	

func _on_bar_texture_health():
	
	stares = Star.LEFT
	shooter_timer.stop()
	coll.set_deferred("disabled", true)
	play = "dead"
	move = 1
	if h != null:
		h.queue_free()


func _on_shooter_timer_timeout():
	if reds == Red.RUN_UN:
		shooter_timer.wait_time = randf_range(5,8)
				
		
		
		stares = Star.WEAPON
		reds = Red.RUN_DOS
		###################################
		h = virus.instantiate()
		shooter.add_child(h) 
		#h.global_position = shooter.global_position
		h.global_position.x -= 450
		h.global_position.y += randf_range(-80, 80)
		h.scale = Vector2(5, 1.5)
		
		
				
	elif reds == Red.RUN_DOS:
		shooter_timer.wait_time = randf_range(1, 2)
		
		
		h.queue_free()
		stares = Star.RUN
		reds = Red.RUN_UN
		
