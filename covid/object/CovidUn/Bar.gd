extends Control

signal texture_health()

@onready var texture_bar := $TextureProgressBar

func _ready():
	$TextureProgressBar/TextureRect3.visible = true
	$TextureProgressBar/TextureRect4.visible = true



func dead() -> void:
	
	texture_bar.value -= 5
	if texture_bar.value <= 0:
		
		
		emit_signal('texture_health')
		
		
		texture_bar.visible = false


func _on_texture_progress_bar_value_changed(value):
	
	if value < 99:
		
		$TextureProgressBar/TextureRect4.visible = false
	else:
		$TextureProgressBar/TextureRect4.visible = true
	
	if value <= 0:
		$TextureProgressBar/TextureRect3.visible = false
	else:
		$TextureProgressBar/TextureRect3.visible = true
