extends Node

var data_naves := { "data_nave" : "nave_01", "data_k" : 0}
var player_nave : String = ""

var e_player : String
var e_flash : String 

func data_flash() -> void:
	if data_naves['data_k'] == 0:
		if data_naves['data_nave'] == "nave_01":
			e_player = "player_01"
			e_flash = "flash_01"
		elif data_naves['data_nave'] == "nave_02":
			e_player = "player_02"
			e_flash = "flash_02"
		elif data_naves['data_nave'] == "nave_03":
			e_player = "player_03"
			e_flash = "flash_03"
	elif data_naves['data_k'] == 1:
		if data_naves['data_nave'] == "nave_01":
			e_player = "player_04"
			e_flash = "flash_04"
		elif data_naves['data_nave'] == "nave_02":
			e_player = "player_05"
			e_flash = "flash_05"
		elif data_naves['data_nave'] == "nave_03":
			e_player = "player_06"
			e_flash = "flash_06"
	else:
		print("error")

func data_nave(o) -> void:
	data_naves['data_nave'] = o
	
func data_k(o) -> void:
	data_naves['data_k'] = o


func data_nave_save() -> String:
	var nave_quit : String
	if data_naves['data_k'] == 0:
		if data_naves['data_nave'] == "nave_01":
			nave_quit = "player_01"
		elif data_naves['data_nave'] == "nave_02":
			nave_quit = "player_02"
		elif data_naves['data_nave'] == "nave_03":		
			nave_quit = "player_03"
	elif data_naves["data_k"] == 1:
		nave_quit = data_naves['data_nave']	
	else:
		print("error")
	
	return nave_quit	
